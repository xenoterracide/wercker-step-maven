NAME
====
maven

VERSION
=======
v0.4.7

SYNOPSIS
========

in `wercker.yml`

    build:
      steps:
        - xenoterracide/maven:
            goals: test
            settings: settings.xml

DESCRIPTION
===========
Runs `mvn` with options.

Example Config
--------------

this is a direct copy and paste of one of mine

~~~
box: maven:latest
build:
  steps:
    - xenoterracide/maven:
        goals: test
~~~

Here's a more complicated example

~~~
box: maven:latest
build:
  steps:
    - xenoterracide/maven:
        name: checkstyle
        goals: checkstyle:check
    - xenoterracide/maven:
        name: compile
        goals: compile
    - xenoterracide/maven:
        name: findbugs
        goals: findbugs:check
    - xenoterracide/maven:
        name: test
        goals: test
        threads: 0.25C
        debug: 1
deploy:
  steps:
    - xenoterracide/maven:
        settings: settings.xml
        goals: deploy
        skiptests: true
~~~

for a `settings.xml` with this setup you can use this, obviously substituting your own server id's.
 I suggest Werckers Organization Settings so that you only have to configure these once, but 
 not in your repository or docker container.

~~~
<settings xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                          http://maven.apache.org/xsd/settings-1.0.0.xsd">
  <servers>
    <server>
      <id>my-releases</id>
      <username>${env.MAVEN_DEPLOY_USER}</username>
      <password>${env.MAVEN_DEPLOY_PASS}</password>
    </server>
    <server>
      <id>my-snapshots</id>
      <username>${env.MAVEN_DEPLOY_USER}</username>
      <password>${env.MAVEN_DEPLOY_PASS}</password>
    </server>
  </servers>
</settings>
~~~

For a full example application you may look at
[this Spring Boot Application](https://app.wercker.com/#applications/54befcb456767b4663000750)

FAQ
===

bad indentation of a sequence at line XXX
-----------------------------------------

Wercker's parser seems to be extremely sensitive, and not good at reporting the actual line/column number the error
occurs on, please ensure that you have a colon after the plugin name, and a four space additional indent on the next
line

    - xenoterracide/maven:
    #####################^ the colon is important
        goals: test
    ####^ you must have a four space indentation

Please specify at least one maven goal
--------------------------------------

This likely means that you've typo'ed the `goals` key, Wercker appears to build it's shell keys by doing an uppercase
of

    uppercase("wercker" + "_" + $plugin_name + "_" + $plugin_parameter_key)

or in our case this likely means that `goals:` is typo-ed


CONTRIBUTING
============
* please remember to update the here and in `wercker-step.yml`
* add a `Changes` entry
* feel free to add yourself to the below contributors list

CONTRIBUTORS
============
* Jan Mynařík


AUTHOR
======
* Caleb Cushing <xenoterracide@gmail.com>

COPYRIGHT AND LICENSE
=====================
This Software is Copyright (c) 2016 by Caleb Cushing.

This is free software, licensed under:
[*The Apache 2.0*](https://www.tldrlegal.com/l/apache2)
