#!/bin/sh
set +o xtrace # disable xtrace
debug $SHELL
if ! type mvn &>/dev/null; then
	error "mvn is not installed"
else
	info "mvn is installed"
	debug "$(mvn -version)"
fi

if [ ! -n "$WERCKER_MAVEN_GOALS" ]; then
	fail "Please specify at least one maven goal"
fi

function disable_xtrace_and_return_status() {
    set +o xtrace
    return $1
}

function run() {
    if [ -n "$WERCKER_MAVEN_DEBUG" ]; then
        if [ "$WERCKER_MAVEN_DEBUG" -ge "1" ]; then
            set -o xtrace
            if [ "$WERCKER_MAVEN_DEBUG" -ge "3" ]; then
                local debug="--debug"
            fi
        fi
    fi

    if [ -e "$WERCKER_MAVEN_SETTINGS" ]; then
       local settings="--settings=${WERCKER_MAVEN_SETTINGS}"
    fi

    if [ -n "$WERCKER_MAVEN_THREADS" ]; then
        local threads="--threads=${WERCKER_MAVEN_THREADS}"
    fi

    if [ -n "$WERCKER_MAVEN_SKIPTESTS" ]; then
        local skiptests="-Dmaven.test.skip=true"
    fi

    mvn --update-snapshots \
        --batch-mode \
        -Dmaven.repo.local=${WERCKER_CACHE_DIR} \
        ${settings} ${debug} ${threads} ${skiptests} \
        ${WERCKER_MAVEN_ARGS} \
        ${WERCKER_MAVEN_GOALS}

   disable_xtrace_and_return_status $?
}

run;
